import java.util.ArrayList;
import java.util.List;

public class LetterFrequencies 
{
	public static char[] mono = new char[]
			{'e','t','a','o','i','n','s','r','h','d','l','u','c','m','f','y',
			 'w','g','p','b','v','k','x','q','j','z'};
	public static String[] di = new String[]
			{"th","he","in","er","an","re","nd","at","on","nt","ha","es","st",
			 "en","ed","to","it","ou","ea","hi","is","or","ti","as","te","et",
			 "ng","of","al","de","se","le","sa","si","ar","ve","ra","ld","ur"};
	
	//scores given while considering possible substituions
	public static int scoreMono = 4;
	public static int scoreDi = 2;
}