import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.io.*;

public class Monoalphabetic 
{
	public static void main(String[] args) throws IOException
	{
		computeSubstituionScores();
		checkAgainstDictionary();
	}
	
	public static void computeSubstituionScores () throws IOException 
	{
		String fileName = "newciphertext.txt";
	    Path path = Paths.get(fileName);
	    Scanner scanner = new Scanner(path);
		String cipherText = "";
		String currentBlob = new String ("");
		List <String> cipherBlobs = new ArrayList <String> (); //Each entry is basically a part of the ciphertext separated by special tokens
																//NOTE: Some entries may be "" also, in the case where there are continuous special characters
		String currentText;
		
		while(scanner.hasNext())
		{
		    currentText = scanner.next();	
		    for (char currentChar: currentText.toCharArray())
		    {
		    	cipherText += currentChar;
			    if (!Character.isLetter(currentChar))
			    {
			    	cipherBlobs.add(currentBlob);
			    	currentBlob = new String ("");
			    }
			    else
			    {
			    	currentBlob += currentChar;
			    }
		    }
		}
		System.out.println("cipherText = \n" + cipherText);
		System.out.println("Blobs");
		
		for (String blob: cipherBlobs)
		{
			System.out.println(blob);
		}
			
		
		//Calculating monAlphabetic frequncies in the given ciphertext
		//NOTE: After this part some letters may not have entries in freqMapMono
		Map <Character,Integer> freqMapMono = new HashMap <Character,Integer>();  
		for ( int i = 0; i < cipherText.length(); i++ )
		{
			char c = cipherText.charAt(i);
			if (Character.isLetter(c))
			{
				Integer freq = freqMapMono.get(new Character(c));
				if ( freq != null )
				{
					freqMapMono.put(c, new Integer( freq + 1 ));
				}
				else
				{
					freqMapMono.put(c, 1);
				}
			}
		}
		
		//Calculating digram frequncies in the given ciphertext
		//NOTE: After this part some digrams may not have entries in freqMapDi
		Map <String, Integer> freqMapDi = new HashMap <String, Integer> ();  
		for (String blob: cipherBlobs)
		{
			for (int i=0; i < blob.length()-1; i++)
			{
				String digram = blob.substring (i, i+2);   //Extracting digrams from each cipher blob
				Integer freq = freqMapDi.get (digram);
				if (freq != null)
				{
					freqMapDi.put (digram, new Integer( freq + 1 ));
				}
				else
				{
					freqMapDi.put (digram, 1);
				}
			}
		}
		
		String[][] sortedFreqMono = sortByValues(freqMapMono);
		String[][] sortedFreqDi = sortByValues(freqMapDi);
		
		for (int i=0; i<sortedFreqMono.length; i++)
		{
			System.out.println (sortedFreqMono[i][0] + " : " + sortedFreqMono [i][1]);
		}
		for (int i=0; i<sortedFreqDi.length; i++)
		{
			System.out.println (sortedFreqDi[i][0] + " : " + sortedFreqDi [i][1]);
		}
		
		
		Integer [][] substitutionScores = new Integer [26][26];   //every row corresponds to a encrypted letter and every column corresponds to an unencrypted letter
		for (int i=0; i<26; i++)
		{
			for (int j=0; j<26; j++)
			{
				substitutionScores [i][j] = 0;
			}
		}
		
		//Giving weights for possible substitutions
		for (int i=0; i<sortedFreqMono.length && i<LetterFrequencies.mono.length; i++)
		{
			Character cipherChar = sortedFreqMono [i][0].charAt(0);
			Character plainChar = LetterFrequencies.mono [i];
			
			substitutionScores [cipherChar - 'a'][plainChar - 'a'] += LetterFrequencies.scoreMono;
		}
		
		for (int i=0; i<sortedFreqDi.length && i<LetterFrequencies.di.length; i++)
		{
			for (int j=0; j<2; j++)
			{
				Character cipherChar = sortedFreqDi [i][0].charAt(j);
				Character plainChar = LetterFrequencies.di [i].charAt(j);
				substitutionScores [cipherChar - 'a'][plainChar - 'a'] += LetterFrequencies.scoreDi;
			}
		}
		
		//Printing out the score table
		System.out.print("  ");
		for (int i=0; i<26; i++)
		{
			System.out.print(" " + ((char) (i + 'a')) + " ");
		}
		System.out.println();
		
		for (int i=0; i<26; i++)
		{
			System.out.print(((char) ('a' + i)) + " ");
			for (int j=0; j<26; j++)
			{
				System.out.print(String.format("%2s", substitutionScores [i][j]) + " ");
			}
			System.out.println();
		}
		char[] decrypted = cipherText.toCharArray();
		for ( int i=0; i<decrypted.length; i++ )
		{
			for (int j = 0; j < 26; j++ )
			{
			if ( decrypted[i] == sortedFreqMono[j][0].charAt(0) )
				{
					decrypted[i] = LetterFrequencies.mono[j];
					break;
				}
			//System.out.println(sortedFreqMono[j][0].charAt(0) + " - " + LetterFrequencies.mono[j]);
			/*decrypted = decrypted.replace(sortedFreqMono[i][0].charAt(0), LetterFrequencies.mono[i]);
			System.out.println(decrypted);*/
			}
		}
		for( int i = 0; i < 26; i++ )
		{
		System.out.println(sortedFreqMono[i][0].charAt(0) + " - " + LetterFrequencies.mono[i]);
		}
		for ( int i=0; i<decrypted.length; i++ )
		{
		System.out.print(decrypted[i]);
		}
	}
		
	
	public static void checkAgainstDictionary () throws IOException
	{
		Scanner scanner = new Scanner(Paths.get("permutations.txt"));
		ArrayList <String[]> permutations = new ArrayList <String[]> (); //Contains all the best permutations in order
																		//Each line contains one permutation
		
		while (scanner.hasNext())
		{
			String line = scanner.nextLine();
			permutations.add(line.split(" "));
		}
		
		/*
		 * TODO: For all the possible sequences in permutations, check against dictionary and see if suitable
		 */
	}
	
	
	//Sorts the given character maps based on frequncy of occurence in descending order
	public static <K extends Comparable,V extends Comparable> String [][] sortByValues(Map<K,V> map)
	{
        List<Map.Entry<K,V>> entries = new LinkedList<Map.Entry<K,V>>(map.entrySet());
      
        Collections.sort(entries, new Comparator<Map.Entry<K,V>>() {
            @Override
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });
      
        String[][] sortedFreq = new String[entries.size()][2];
        int i=0;
        
        for(Map.Entry<K,V> entry: entries)
	    {
        	sortedFreq[i][0] = entry.getKey().toString();
        	sortedFreq[i][1] = entry.getValue().toString();
        	i++;
	    }
        return sortedFreq;
	}
	
	//Brute force computation of permuatations
	/*
	public static void computePermutations (int level, ArrayList <Integer> currSequence, ArrayList <ArrayList <Integer>> permutations)
	{
		if (level == 0)
		{
			permutations.add(currSequence);
			for (Integer value: currSequence)
			{
				System.out.print(value + " ");
			}
			System.out.println();
			return;
		}
		else
		{
			for (int i=1; i<=26; i++)
			{
				if (!currSequence.contains(i))
				{
					ArrayList<Integer> currSequenceNew = new ArrayList<Integer>(currSequence);
					currSequenceNew.add(i);
					computePermutations(level-1, currSequenceNew, permutations);
				}
			}
			return;
		}
	}*/
}